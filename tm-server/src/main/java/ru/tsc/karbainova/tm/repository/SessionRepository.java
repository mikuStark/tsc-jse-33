package ru.tsc.karbainova.tm.repository;

import ru.tsc.karbainova.tm.api.repository.ISessionRepository;
import ru.tsc.karbainova.tm.model.Session;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
}
